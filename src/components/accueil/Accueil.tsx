import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import axios from 'axios';
import { API_URL } from '../../env/Env';

interface IState {
    clients: any[];
}

export default class Home extends React.Component<RouteComponentProps, IState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { clients: [] }
    }
    public componentDidMount(): void {
        axios.get(API_URL+`/clients`).then(data => {
            this.setState({ clients: data.data.data.clients })
            console.log("test");
        })
    }
    public deleteCustomer(id: number) {
        axios.delete(API_URL+`/clients/${id}`).then(data => {
            const index = this.state.clients.findIndex(client => client.id === id);
            this.state.clients.splice(index, 1);
            this.props.history.push('/');
        })
    }
    public render() {
        const clients = this.state.clients;
        return (
            <div>
                {clients.length === 0 && (
                    <div className="text-center">
                        <h2>Aucun client trouvé pour le moment</h2>
                    </div>
                )}
                <div className="container">
                    <div className="row">
                        <table className="table table-bordered">
                            <thead className="thead-light">
                                <tr>
                                    <th scope="col">Prénom</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Téléphone</th>
                                    <th scope="col">Adresse</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {clients && clients.map(client =>
                                    <tr key={client.id}>
                                        <td>{client.prenom}</td>
                                        <td>{client.nom}</td>
                                        <td>{client.email}</td>
                                        <td>{client.tel}</td>
                                        <td>{client.adresse}</td>
                                        <td>
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="btn-group" style={{ marginBottom: "20px" }}>
                                                    {
                                                        //<Link to={`edit/${client.id}`} className="btn btn-sm btn-outline-secondary">Editer</Link>
                                                    }
                                                    <button className="btn btn-sm btn-outline-secondary" onClick={() => this.deleteCustomer(client.id)}>Supprimer</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                        <Link to={'/create'}>  
                            <button className="btn btn-success">
                                    Créer un nouveau client
                            </button>
                         </Link>
                       
                    </div>
                </div>
            </div>
        )
    }
}