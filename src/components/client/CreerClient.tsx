import * as React from 'react';
import axios from 'axios';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { API_URL } from '../../env/Env';

export interface IValues {
    prenom: string,
    nom: string,
    email: string,
    tel: string,
    adresse: string
}
export interface IFormState {
    [key: string]: any;
    values: IValues[];
    submitSuccess: boolean;
    loading: boolean;
}
class Create extends React.Component<RouteComponentProps, IFormState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            prenom: '',
            nom: '',
            email: '',
            tel: '',
            adresse: '',
            values: [],
            loading: false,
            submitSuccess: false,
        }
    }

    private processFormSubmission = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        this.setState({ loading: true });
        const formData = {
            prenom: this.state.prenom,
            nom: this.state.nom,
            email: this.state.email,
            tel: this.state.tel,
            adresse: this.state.adresse
        }
        this.setState({ submitSuccess: true, loading: false, values:[...this.state.values, formData]});
        
        axios.post(API_URL+`/clients`, formData).then(data => [
            setTimeout(() => {
                this.props.history.push('/');
            }, 1500)
        ]);
    }

    private handleInputChanges = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        })
    }

    public render() {
        const { submitSuccess, loading } = this.state;
        return (
            <div>
                <div className={"col-md-12 form-wrapper"}>
                    <h2> Créer un client </h2>
                    {!submitSuccess && (
                        <div className="alert alert-info" role="alert">
                            Remplir le formulaire ci-dessous afin de créer un nouveau client
                    </div>
                    )}
                    {submitSuccess && (
                        <div className="alert alert-info" role="alert">
                            La création du client est un succès !
                            </div>
                    )}
                    <form id={"create-post-form"} onSubmit={this.processFormSubmission} noValidate={true}>
                        <div className="form-group col-md-12">
                            <label htmlFor="first_name"> Prénom </label>
                            <input type="text" id="first_name" onChange={(e) => this.handleInputChanges(e)} name="prenom" className="form-control" placeholder="Saisir le prénom du client" />
                        </div>
                        <div className="form-group col-md-12">
                            <label htmlFor="last_name"> Nom </label>
                            <input type="text" id="last_name" onChange={(e) => this.handleInputChanges(e)} name="nom" className="form-control" placeholder="Saisr le nom du client" />
                        </div>
                        <div className="form-group col-md-12">
                            <label htmlFor="email"> Email </label>
                            <input type="email" id="email" onChange={(e) => this.handleInputChanges(e)} name="email" className="form-control" placeholder="Saisir l'adresse mail du client" />
                        </div>
                        <div className="form-group col-md-12">
                            <label htmlFor="phone"> Téléphone </label>
                            <input type="text" id="phone" onChange={(e) => this.handleInputChanges(e)} name="tel" className="form-control" placeholder="Saisr le téléphone du client" />
                        </div>
                        <div className="form-group col-md-12">
                            <label htmlFor="address"> Adresse </label>
                            <input type="text" id="adresse" onChange={(e) => this.handleInputChanges(e)} name="adresse" className="form-control" placeholder="Saisir l'adresse du client" />
                        </div>
                        <div className="form-group col-md-4 pull-right">
                            <button className="btn btn-success" type="submit">
                                Créer client
                            </button>
                            {loading &&
                                <span className="fa fa-circle-o-notch fa-spin" />
                            }
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
export default withRouter(Create)