import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import axios from 'axios';
import { API_URL } from '../../env/Env';

export interface IValues {
    [key: string]: any;
}
export interface IFormState {
    id: number,
    client: any;
    values: IValues[];
    submitSuccess: boolean;
    loading: boolean;
}
class EditerClient extends React.Component<RouteComponentProps<any>, IFormState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            client: {},
            values: [],
            loading: false,
            submitSuccess: false,
        }
    }
    public componentDidMount(): void {
        axios.get(API_URL+`/clients/${this.state.id}`).then(data => {
            this.setState({ client: data.data.data.client[0] });
        })
    }

    private processFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
        e.preventDefault();
        this.setState({ loading: true });
        const formData = {
            prenom: this.state.client.prenom,
            nom: this.state.client.nom,
            email: this.state.client.email,
            tel: this.state.client.tel,
            adresse: this.state.client.adresse
        }
        axios.post(API_URL+`/clients/${this.state.id}`, formData).then(data => {
            this.setState({ submitSuccess: true, loading: false })
            setTimeout(() => {
                this.props.history.push('/');
            }, 1500)
        })
    }

    private setValues = (values: IValues) => {
        this.setState({ values: { ...this.state.values, ...values } });
    }
    private handleInputChanges = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.setValues({ [e.currentTarget.id]: e.currentTarget.value })
    }
    public render() {
        const { submitSuccess, loading } = this.state;
        return (
            <div className="App">
                    <div>
                        <div>
                            <div className={"col-md-12 form-wrapper"}>
                                <h2> Editer un client </h2>
                                {submitSuccess && (
                                    <div className="alert alert-info" role="alert">
                                        la modification des informations du client est un succès </div>
                                )}
                                <form id={"create-post-form"} onSubmit={this.processFormSubmission} noValidate={true}>
                                    <div className="form-group col-md-12">
                                        <label htmlFor="first_name"> Prénom </label>
                                        <input type="text" id="first_name" defaultValue={this.state.client.prenom} onChange={(e) => this.handleInputChanges(e)} name="client.prenom" className="form-control" placeholder="Saisir le prénom du client" />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <label htmlFor="last_name"> Nom </label>
                                        <input type="text" id="last_name" defaultValue={this.state.client.nom} onChange={(e) => this.handleInputChanges(e)} name="nom" className="form-control" placeholder="Saisr le nom du client" />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <label htmlFor="email"> Email </label>
                                        <input type="email" id="email" defaultValue={this.state.client.email} onChange={(e) => this.handleInputChanges(e)} name="email" className="form-control" placeholder="Saisir l'adresse email du client" />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <label htmlFor="phone"> Téléphone </label>
                                        <input type="text" id="phone" defaultValue={this.state.client.tel} onChange={(e) => this.handleInputChanges(e)} name="tel" className="form-control" placeholder="Saisir le téléphone du client" />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <label htmlFor="address"> Adresse </label>
                                        <input type="text" id="adresse" defaultValue={this.state.client.adresse} onChange={(e) => this.handleInputChanges(e)} name="adresse" className="form-control" placeholder="Saisir l'adresse du client" />
                                    </div>
                                    <div className="form-group col-md-4 pull-right">
                                        <button className="btn btn-success" type="submit">
                                            Editer client </button>
                                        {loading &&
                                            <span className="fa fa-circle-o-notch fa-spin" />
                                        }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        )
    }



}
export default withRouter(EditerClient)