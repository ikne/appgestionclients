require('dotenv').config({path: __dirname + '/.env'})

export const API_URL = process.env.APIURL || 'https://apigestionclients-prod.herokuapp.com';